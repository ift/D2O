#!/bin/bash

apt-get update -qy
apt-get install -y openmpi-bin libopenmpi-dev
pip install mpi4py

D2O - Distributed Data Object
=============================

`A distributed data object for parallel high-performance computing in Python`


Summary
-------

D2O is a Python module for cluster-distributed multi-dimensional 
numerical arrays. It acts as a layer of abstraction between the algorithm code 
and the data-distribution logic. The main goal is to achieve usability without 
losing numerical performance and scalability. D2O's global interface is similar
to the one of a `numpy.ndarray`, whereas the cluster node's local data is 
directly accessible for use in customized high-performance modules. D2O is 
written in pure Python which makes it portable and easy to use and modify.
Expensive operations are carried out by dedicated external libraries like 
`numpy` and `mpi4py`. The performance of D2O is on a par to numpy for serial 
applications and scales well when moving to an MPI cluster. D2O is open-source 
software available under the GNU General Public License v3 (GPL-3). 

Installation
------------

Requirements
............

*   `Python <http://www.python.org/>`_ (v2.7.x or v3)
*   `NumPy <http://www.numpy.org/>`_ 
*   `mpi4py <https://bitbucket.org/mpi4py/mpi4py>`_ **optional**

Installation on Ubuntu
......................

Optionally install mpi4py ::

        sudo pip install mpi4py

Install D2O ::

        git clone https://gitlab.mpcdf.mpg.de/ift/D2O.git
        cd D2O
        sudo python setup.py install 
        cd ..

